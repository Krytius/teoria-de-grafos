module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        meta: {
            sshProduction: {
                key: "",
                ssh: "ubuntu@ip",
                path: "/home/ubuntu/www/"
            },
            sshTest: {
                key: "",
                ssh: "ubuntu@ip",
                path: "/home/ubuntu/www/dev/"
            },
            messageCommit: ""
        },

        gitadd: {
            build: {
                options: {
                    force: true,
                    all: true
                }
            }
        },

        gitcommit: {
            build: {
                options: {
                    noVerify: true,
                    noStatus: false,
                    message: '<%= meta.messageCommit %>',
                    cwd: "./"
                }
            }
        },

        gitpush: {
            build: {
                options: {
                    remote: 'origin'
                }
            }
        },

        rsync: {
            options: {
                args: ["--verbose"],
                recursive: true,
                syncDestIgnoreExcl: true
            },

            test: {
                options: {
                    privateKey: "<%= meta.sshTest.key %>",
                    src: "<%= meta.wp %>",
                    exclude: ["<%= meta.production %>"],
                    dest: "<%= meta.sshTest.path %>",
                    host: "<%= meta.sshTest.ssh %>"
                }
            },

            build: {
                options: {
                    privateKey: "<%= meta.sshProduction.key %>",
                    src: "<%= meta.production %>",
                    dest: "<%= meta.sshProduction.path %>",
                    host: "<%= meta.sshProduction.ssh %>"
                }
            }
        },

        prompt: {
            git: {
                options: {
                    questions: [
                        {
                            config: "meta.messageCommit",
                            type: "input",
                            message: 'Commentário commit:',
                        }
                    ]
                }
            },
        },

    });

    grunt.registerTask('git', ["prompt:git", "gitadd", "gitcommit", "gitpush"]);
    grunt.registerTask('deploy', ["git", "rsync:build"]);
};