<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('system');
});

Route::resource('/Distancia', "DistanciaController");
Route::resource('/Profundidade', "ProfundidadeController");
Route::resource('/Largura', "LarguraController");
Route::resource('/Menorcaminho', "MenorCaminhoController");
Route::resource('/Prim', "PrimController");
Route::resource('/Kruskal', "KruskalController");