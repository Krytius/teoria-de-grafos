<?php
/**
 * Created by PhpStorm.
 * User: krytius
 * Date: 22/11/15
 * Time: 18:52
 */

namespace Krytius\Grafos;


class Distancia
{

    /**
     * @param $arestas
     * @param $vertices
     * @return int|string
     */
    public static function pontos($arestas, $vertices)
    {
        $distancia = 0;
        for ($i = 0; $i < count($vertices); $i++) {

            if (!isset($vertices[$i + 1])) {
                break;
            }

            $get = 0;
            foreach ($arestas as $key => $val) {
                if ($val["De"] == $vertices[$i] && $val["Para"] == $vertices[$i + 1]) {
                    $get = intval($val["Peso"]);
                } else if ($val["De"] == $vertices[$i + 1] && $val["Para"] == $vertices[$i]) {
                    $get = intval($val["Peso"]);
                }
            }

            if ($get) {
                $distancia += $get;
            } else {
                return "Aresta não existe de " . $vertices[$i] . " para " . $vertices[$i + 1];
            }
        }

        return $distancia;
    }


}