<?php

namespace Krytius\Grafos;

use Illuminate\Support\ServiceProvider;

class GrafosServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Krytius\Grafos\Distancia'
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Distância
        $this->app->singleton('Krytius\Grafos\Distancia', function () {
            return new Distancia();
        });

        // Profundidade
        $this->app->singleton('Krytius\Grafos\Profundidade', function () {
            return new Profundidade();
        });

        // Largura
        $this->app->singleton('Krytius\Grafos\Largura', function () {
            return new Largura();
        });

        // Menorcaminho
        $this->app->singleton('Krytius\Grafos\Menorcaminho', function () {
            return new Menorcaminho();
        });

        // Prim
        $this->app->singleton('Krytius\Grafos\Prim', function () {
            return new Prim();
        });

        // Kruskal
        $this->app->singleton('Krytius\Grafos\Kruskal', function () {
            return new Kruskal();
        });

    }
}
