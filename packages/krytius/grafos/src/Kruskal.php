<?php
/**
 * Created by PhpStorm.
 * User: krytius
 * Date: 22/11/15
 * Time: 20:01
 */

namespace Krytius\Grafos;


class Kruskal
{

    public function pontos($arestas)
    {

        usort($arestas, function ($a, $b) {
            return $a['Peso'] > $b['Peso'];
        });

        $soma = 0;
        $arrayVisitados = array();
        $array = array();

        foreach ($arestas as $k => $v) {
            if (in_array($v["Para"], $arrayVisitados)) {
                continue;
            }

            array_push($array, $v);
            array_push($arrayVisitados, $v["Para"]);

            $soma += $v["Peso"];

        }

        return array(
            "peso" => $soma,
            "vertices" => $array
        );
    }

}