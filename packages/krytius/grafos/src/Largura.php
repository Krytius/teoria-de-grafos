<?php
/**
 * Created by PhpStorm.
 * User: krytius
 * Date: 22/11/15
 * Time: 20:01
 */

namespace Krytius\Grafos;


class Largura
{

    public function pontos($arestas, $vertices)
    {
        $prox = $this->getDe($vertices);
        $relaxamentos = [
            [$prox]
        ];

        do {

            $obj = array();
            foreach ($arestas as $k => $v) {

                if ($v["De"] == $prox) {
                    array_push($obj, $v["Para"]);
                }

            }

            $prox = $obj[0];

            if ($prox == $this->getPara($vertices)) {
                array_push($obj, $prox);
            } else if ($obj[count($obj) - 1] == $this->getPara($vertices)) {
                array_push($obj, $obj[count($obj) - 1]);
            }

            array_push($relaxamentos, $obj);
        } while ($prox != $this->getPara($vertices));


        return $relaxamentos;
    }


    private function getDe($vertice)
    {
        return $vertice[0];
    }

    private function getPara($vertice)
    {
        return $vertice[1];
    }
}