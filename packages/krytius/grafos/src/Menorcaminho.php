<?php
/**
 * Created by PhpStorm.
 * User: krytius
 * Date: 22/11/15
 * Time: 20:01
 */

namespace Krytius\Grafos;

use Krytius\Grafos\Distancia;


class Menorcaminho
{

    public function pontos($arestas, $vertices)
    {
        $relaxamentos = [];

        $prox = $this->getDe($vertices);

        foreach ($arestas as $key => $val) {
            if ($val["De"] == $prox) {
                array_push($relaxamentos, [$val["De"], $val["Para"]]);
            }
        }


        do {
            foreach ($relaxamentos as $key => $val) {
                $cotador = 0;
                foreach ($arestas as $key2 => $val2) {
                    if ($val[count($val) - 1] == $val2["De"]) {
                        if ($cotador == 0) {
                            array_push($relaxamentos[$key], $val2["Para"]);
                            $cotador++;
                        } else {
                            $obj = array();
                            for ($i = 0; $i < count($relaxamentos[$key]) - 1; $i++) {
                                array_push($obj, $relaxamentos[$key][$i]);
                            }

                            array_push($obj, $val2["Para"]);
                            array_push($relaxamentos, $obj);
                        }
                    }
                }
            }

            $continue = false;
            foreach ($relaxamentos as $key => $val) {
                if ($val[count($val) - 1] != $this->getPara($vertices)) {
                    $continue = true;
                    break;
                }
            }
        } while ($continue);

        $menorDistancia = $relaxamentos[0];
        $distancia = Distancia::pontos($arestas, $relaxamentos[0]);

        foreach ($relaxamentos as $key => $val) {
            $dist = Distancia::pontos($arestas, $val);
            if ($dist < $distancia) {
                $distancia = $dist;
                $menorDistancia = $val;
            }
        }

        return array(
            "distancia" => $distancia,
            "vertices" => $menorDistancia
        );
    }

    private function getDe($vertice)
    {
        return $vertice[0];
    }

    private function getPara($vertice)
    {
        return $vertice[1];
    }


}