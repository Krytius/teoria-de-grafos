<?php
/**
 * Created by PhpStorm.
 * User: krytius
 * Date: 22/11/15
 * Time: 20:01
 */

namespace Krytius\Grafos;


class Prim
{

    public function pontos($arestas, $vertices)
    {
        $origem = $this->getDe($vertices);
        $arestasEscolhidas = array($origem);
        $escolhidas = array();


        do {
            $continue = false;
            $temp = array();


            foreach ($arestasEscolhidas as $k2 => $v2) {
                foreach ($arestas as $k => $v) {
                    if (!in_array($v["Para"], $arestasEscolhidas)) {
                        if ($v2 == $v["De"]) {
                            array_push($temp, $v);
                        }
                    }
                }
            }

            if (count($temp) > 0) {
                $distancia = $temp[0]["Peso"];
                $selecionado = $temp[0];
                foreach ($temp as $k => $v) {
                    if ($distancia > $v["Peso"]) {
                        $distancia = $v["Peso"];
                        $selecionado = $v;
                    }
                }

                array_push($arestasEscolhidas, $selecionado["Para"]);
                array_push($escolhidas, $selecionado);
                $continue = true;
            }

        } while ($continue);

        return $escolhidas;
    }

    private function getDe($vertice)
    {
        return $vertice[0];
    }
}