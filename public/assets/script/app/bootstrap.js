/* global angular */

var app = angular.module('project', ['ui.router']);

app.config(function ($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise("/");
    $stateProvider
            .state('home', {
                url: "/",
                templateUrl: "views/home.html",
                controller: "HomeController"
            })
            .state('outra', {
                url: "/outra",
                templateUrl: "views/outra.html",
                controller: "OutraController"
            });
});