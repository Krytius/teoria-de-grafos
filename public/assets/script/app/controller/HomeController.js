/* global app */

app.controller("HomeController", function ($scope, GrafoService, ArestasService, ComandosService, LeituraService) {

    var textArea;
    var alfabeto = ["GRAFO", "ARESTAS", "COMANDOS"];

    /**
     *
     * @type {{vertices: Array, direcionado: number, pesos: number}}
     */
    var Grafo = {
        vertices: [],
        direcionado: 0,
        pesos: 0
    };

    /**
     * {
     *      De: 0
     *      Para: 1
     *      Peso: 10
     * }
     * @type {Array}
     */
    var Arestas = [];

    /**
     * {
     *      Comando:
     *      Vertices: []
     * }
     * @type {Array}
     */
    var Comandos = [];


    $scope.iniciarGrafo = function () {
        var Grafo = {
            vertices: [],
            direcionado: 0,
            pesos: 0
        };
        Arestas = [];
        Comandos = [];
        textArea = document.getElementById("grafo").value;


        var tmp = "";
        var contador = 0;
        var contadorPontoVirgula = 0;
        var processo = "";

        var count = textArea.length;
        for (var i = 0; i < count; i++) {
            tmp += textArea[i];

            if (contador != 0) {
                switch (processo) {
                    case grafo():
                        if (textArea[i].charCodeAt() == 10) {
                            contador--;
                        }
                        if (contador == 0) {
                            Grafo = GrafoService.init(tmp);
                            tmp = "";
                        }
                        break;
                    case arestas():
                        if (textArea[i].charCodeAt() == 59) {
                            contadorPontoVirgula--;
                        }
                        if (textArea[i].charCodeAt() == 10) {

                            if (contadorPontoVirgula == 0) {
                                contador--;
                            }
                        }
                        if (contador == 0) {
                            Arestas = ArestasService.init(tmp);
                            tmp = "";
                        }
                        break;
                    case comandos():
                        if (count - 1 === i) {
                            Comandos = ComandosService.init(tmp);
                            tmp = "";
                        }
                        break;

                }


            } else {
                switch (tmp) {
                    case grafo():
                        contador = 4;
                        tmp = "";
                        processo = grafo();
                        break;
                    case arestas():
                        contador = 1;
                        contadorPontoVirgula = 1;
                        tmp = "";
                        processo = arestas();
                        break;
                    case comandos():
                        contador = 1;
                        tmp = "";
                        processo = comandos();
                        break;
                }
            }
        }

        LeituraService.init(Grafo, Arestas, Comandos);
    };

    var grafo = function () {
        return alfabeto[0];
    };

    var arestas = function () {
        return alfabeto[1];
    };

    var comandos = function () {
        return alfabeto[2];
    };


    var init = function () {
        $('textarea').textareaLinesNumbers();
    };

    init();

});