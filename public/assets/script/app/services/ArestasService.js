app.factory('ArestasService', function () {

    var service = {};
    var comment = false;

    /**
     * Leitura das linhas do Grafo
     * @param tmp
     * @returns {{vertices: Array, direcionado: number, pesos: number}}
     */
    service.init = function (tmp) {
        var arestas = [];
        var count = tmp.length;

        var charTmp = "";
        var contador = 0;
        var obj = {};

        for (var i = 0; i < count; i++) {
            var char = tmp[i];

            if (char.charCodeAt() === 10) {
                comment = false;
                continue;
            }

            if (comment) {
                continue;
            }

            if (char.charCodeAt() === 32 || char.charCodeAt() === 44 || char.charCodeAt() === 59) {
                if (charTmp != "") {
                    switch (contador) {
                        case 0:
                            obj.De = charTmp;
                            break;
                        case 1:
                            obj.Para = charTmp;
                            break;
                        case 2:
                            obj.Peso = charTmp;
                            break;
                    }
                    contador++;
                    charTmp = "";

                    if (char.charCodeAt() !== 44 && char.charCodeAt() !== 59) {
                        continue;
                    }
                }
                charTmp = "";
            }

            if (char.charCodeAt() === 44 || char.charCodeAt() === 59) {
                arestas.push(obj);
                obj = {};
                contador = 0;
                continue;
            }

            if (char.charCodeAt() === 47 && tmp[i + 1].charCodeAt() == 47) {
                comment = true;
                continue;
            }

            charTmp += tmp[i];
        }

        return arestas;

    };


    return service;
});