app.factory('ComandosService', function () {

    var service = {};
    var comment = false;

    /**
     * Leitura das linhas do Grafo
     * @param tmp
     * @returns {{vertices: Array, direcionado: number, pesos: number}}
     */
    service.init = function (tmp) {
        var comandos = [];
        var count = tmp.length;

        var charTmp = "";
        var contador = 0;
        var obj = {
            Comando: "",
            Vertices: []
        };

        for (var i = 0; i < count; i++) {
            var char = tmp[i];

            if (char.charCodeAt() === 10) {
                comment = false;
                continue;
            }

            if (comment) {
                continue;
            }

            if ((char.charCodeAt() === 32 || char.charCodeAt() === 59) && (tmp[i + 1] !== "C")) {
                if (charTmp !== "") {
                    if (contador == 0) {
                        obj.Comando = charTmp;
                    } else {
                        obj.Vertices.push(charTmp);
                    }

                    contador++;
                    charTmp = "";

                    if (char.charCodeAt() === 59) {
                        contador = 0;
                        comandos.push(obj);
                        obj = {
                            Comando: "",
                            Vertices: []
                        };
                    }
                }

                continue;
            }

            if (char.charCodeAt() === 47 && tmp[i + 1].charCodeAt() == 47) {
                comment = true;
                continue;
            }

            charTmp += tmp[i];
        }

        return comandos;

    };


    return service;
});