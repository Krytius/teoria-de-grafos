app.factory('GrafoService', function () {

    var service = {};
    var comment = false;

    /**
     * Leitura das linhas do Grafo
     * @param tmp
     * @returns {{vertices: Array, direcionado: number, pesos: number}}
     */
    service.init = function (tmp) {
        var vertice = [];
        var direcionado = false;
        var pesos = false;
        var count = tmp.length;


        var charTmp = "";
        var contador = 0;


        for (var i = 0; i < count; i++) {
            var char = tmp[i];

            if (char.charCodeAt() === 10) {
                comment = false;
                continue;
            }

            if (comment) {
                continue;
            }

            if (char.charCodeAt() === 32 || char.charCodeAt() === 59) {
                if (charTmp != "") {
                    switch (contador) {
                        case 0:
                            vertice.push(charTmp);
                            break;
                        case 1:
                            direcionado = charTmp;
                            break;
                        case 2:
                            pesos = charTmp;
                            break;
                    }
                }

                if (char.charCodeAt() === 59) {
                    contador++;
                }


                charTmp = "";
                continue;
            }

            if (char.charCodeAt() === 47 && tmp[i + 1].charCodeAt() == 47) {
                comment = true;
                continue;
            }

            charTmp += tmp[i];
        }

        return {
            vertices: vertice,
            direcionado: (direcionado === "true") ? 1 : 0,
            pesos: (pesos === "true") ? 1 : 0
        };

    };


    return service;

});
