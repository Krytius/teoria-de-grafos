app.factory('InterfaceService', function () {

    var service = {};

    service.init = function (Grafo, Arestas) {
        var elem = $("#interface");

        var count = Grafo.vertices.length;
        var i = 0;
        var altura = 0;
        console.log(Arestas);

        for (var key in Grafo.vertices) {
            var obj = Grafo.vertices[key];

            if (Math.floor(count / 2) == i) {
                altura += 100;
            }


            var div = $("<div/>").attr("y", altura + 25).attr("x", ((i % 2) * 100) + 25).attr("v", obj);
            div.css({
                top: altura,
                left: (i % 2) * 100,
                "border": "1px solid red",
                width: 50,
                height: 50,
                position: "absolute",
                padding: "18px",
                "text-align": "center",
                "z-index": 1,
                "background": "white"
            });
            div.html(obj);
            elem.append(div);
            i++;
        }


        for (var key in Arestas) {
            var obj = Arestas[key];
            var de = $("[v='" + parseInt(obj.De) + "']");
            var para = $("[v='" + parseInt(obj.Para) + "']");
            console.log(de, para);

            console.log(parseInt(para.attr("x") - de.attr("x")), parseInt(para.attr("y") - de.attr("y")));

            var div = $("<div/>");
            div.css({
                width: (parseInt(para.attr("x") - de.attr("x")) > 0) ? parseInt(para.attr("x") - de.attr("x")) : 15,
                height: (parseInt(para.attr("y") - de.attr("y")) > 0) ? parseInt(para.attr("y") - de.attr("y")) : 15,
                position: "absolute",
                left: parseInt(de.attr("x")),
                top: parseInt(de.attr("y")),
                "border-bottom": "1px solid black",
                "border-left": "1px solid black",
                "text-align": "center"
            });
            div.html(obj.Peso);


            elem.append(div);
        }


    };


    return service;
});