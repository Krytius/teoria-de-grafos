app.factory('LeituraService', function ($http, InterfaceService) {

    var service = {};
    var alfabeto = ["DISTANCIA", "PROFUNDIDADE", "LARGURA", "MENOR CAMINHO", "PRIM", "KRUSKAL"];
    var grafo;
    var arestas;

    service.init = function (Grafo, Arestas, Comandos) {
        grafo = Grafo;
        arestas = Arestas;

        // Inicialização da Interface
        InterfaceService.init(grafo, arestas);

        var count = Comandos.length;
        for (var i = 0; i < count; i++) {
            var comando = Comandos[i].Comando;
            var vertices = Comandos[i].Vertices;

            switch (comando) {
                case alfabeto[0]:
                    getDistancia(vertices);
                    break;

                case alfabeto[1]:
                    getProfundidade(vertices);
                    break;

                case alfabeto[2]:
                    getLargura(vertices);
                    break;

                case alfabeto[3]:
                    getMenorCaminho(vertices);
                    break;

                case alfabeto[4]:
                    getPrim(vertices);
                    break;

                case alfabeto[5]:
                    getKruskal(vertices);
                    break;
            }
        }
    };

    var getDistancia = function (vertices) {
        $http
            .post("/Distancia", {
                Grafo: grafo,
                Arestas: arestas,
                Vertices: vertices
            })
            .then(callbackDistancia);
    };

    var callbackDistancia = function (resp) {
        var div = $("<div/>").html("DISTANCIA " + resp.data.vertices.join(" ") + " :");
        var div2 = $("<div/>").html(resp.data.result);
        $(".panel-body").eq(1).append(div);
        $(".panel-body").eq(1).append(div2);
    };

    var getProfundidade = function (vertices) {
        $http
            .post("/Profundidade", {
                Grafo: grafo,
                Arestas: arestas,
                Vertices: vertices
            })
            .then(callbackProfundidade);
    };

    var callbackProfundidade = function (resp) {
        var div = $("<div/>").html("PROFUNDIDADE " + resp.data.vertices.join(" ") + " :");
        $(".panel-body").eq(1).append(div);

        for (var key in resp.data.result) {
            var obj = resp.data.result[key];

            var div = $("<div/>").html(obj.join(" "));
            $(".panel-body").eq(1).append(div);
        }
    };

    var getLargura = function (vertices) {
        $http
            .post("/Largura", {
                Grafo: grafo,
                Arestas: arestas,
                Vertices: vertices
            })
            .then(callbackLargura);
    };

    var callbackLargura = function (resp) {
        var div = $("<div/>").html("LARGURA " + resp.data.vertices.join(" ") + " :");
        $(".panel-body").eq(1).append(div);

        for (var key in resp.data.result) {
            var obj = resp.data.result[key];

            var div = $("<div/>").html(obj.join(" "));
            $(".panel-body").eq(1).append(div);
        }
    };

    var getMenorCaminho = function (vertices) {
        $http
            .post("/Menorcaminho", {
                Grafo: grafo,
                Arestas: arestas,
                Vertices: vertices
            })
            .then(callbackMenorcaminho);
    };

    var callbackMenorcaminho = function (resp) {
        var div = $("<div/>").html("MENOR CAMINHO " + resp.data.vertices.join(" ") + " :");
        $(".panel-body").eq(1).append(div);

        var div = $("<div/>").html(resp.data.result.vertices.join(" "));
        $(".panel-body").eq(1).append(div);

        var div = $("<div/>").html(resp.data.result.distancia);
        $(".panel-body").eq(1).append(div);
    };

    var getPrim = function (vertices) {
        $http
            .post("/Prim", {
                Grafo: grafo,
                Arestas: arestas,
                Vertices: vertices
            })
            .then(callbackPrim);
    };

    var callbackPrim = function (resp) {
        var div = $("<div/>").html("PRIM " + resp.data.vertices.join(" ") + " :");
        $(".panel-body").eq(1).append(div);


        var peso = 0;
        for (var key in resp.data.result) {
            var obj = resp.data.result[key];

            var div = $("<div/>").html(obj.De + " " + obj.Para + " " + obj.Peso + ((key == resp.data.result.length - 1) ? ";" : ","));
            $(".panel-body").eq(1).append(div);

            peso += parseInt(obj.Peso);
        }

        var div = $("<div/>").html(peso);
        $(".panel-body").eq(1).append(div);
    };

    var getKruskal = function (vertices) {
        $http
            .post("/Kruskal", {
                Grafo: grafo,
                Arestas: arestas,
                Vertices: vertices
            })
            .then(callbackKruskal);
    };

    var callbackKruskal = function (resp) {
        var div = $("<div/>").html("KRUSKAL " + resp.data.vertices.join(" ") + " :");
        $(".panel-body").eq(1).append(div);

        for (var key in resp.data.result.vertices) {
            var obj = resp.data.result.vertices[key];

            var div = $("<div/>").html(obj.De + " " + obj.Para + " " + obj.Peso + ((key == resp.data.result.vertices.length - 1) ? ";" : ","));
            $(".panel-body").eq(1).append(div);
        }

        var div = $("<div/>").html(resp.data.result.peso);
        $(".panel-body").eq(1).append(div);
    };


    return service;
});