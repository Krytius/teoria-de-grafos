## Grafos

Projeto de Grafos da aula de teoria da computação

## Distancia

Cálculo das somas das arestas.

## Profundidade

Os vétices descobertos são inseridos no começo da fronteira

## Largura

Os vertices descobertos são inseridos no final da fronteira

## Menor Caminho

Menor caminho percorrido de uma origem a um destino

## Prim



## Kruskal


(⋆⋆⋆) Algoritmos de Prim e Kruskal mencionados previamente na descrição do trabalho prático
(⋆) Documente o trabalho utilizando um arquivo do tipo Markdown (README.md)
(⋆) Utilize alguma ferramenta de versionamento de código (SVN, Git, etc.) durante TODA a realização do trabalho prático
(⋆) Disponibilize o seu código em um repositório público (Bitbucket, Github, etc.)
(⋆) Utilize alguma ferramenta de automação (Maven, Ivy, Gradle, Grunt, etc.) para facilitar o processo de compilação e distribuição da sua biblioteca
(⋆⋆⋆) Implemente um serviço web em qualquer linguagem de programação que exponha sua biblioteca como uma API REST


