<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Projeto Admin</title>
    <link href="<?php echo asset("assets/style/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset("assets/style/londinium-theme.min.css"); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset("assets/style/styles.min.css"); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset("assets/style/icons.min.css"); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo asset("assets/style/custom.css"); ?>" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

</head>
<body class="sidebar-wide" ng-app="project">

<!-- Navbar -->
<div class="navbar navbar-inverse" role="navigation">

    <div class="navbar-header">
        <a class="navbar-brand" href="#">
            Teoria dos Grafos
        </a>
    </div>
</div>
<!-- /navbar -->

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div ui-view></div>
    <!-- /page content -->

</div>
<!-- /page container -->
</body>

<!-- Script -->
<script src="<?php echo asset("assets/script/lib/jquery.js"); ?>"></script>
<script src="<?php echo asset("assets/script/lib/textareaLines.js"); ?>"></script>
<script src="<?php echo asset("assets/script/lib/angular.min.js"); ?>"></script>
<script src="<?php echo asset("assets/script/lib/angular-ui-router.min.js"); ?>"></script>

<!-- Application -->
<script src="<?php echo asset("assets/script/app/bootstrap.js"); ?>"></script>

<!-- Service -->
<script src="<?php echo asset("assets/script/app/services/GrafoService.js"); ?>"></script>
<script src="<?php echo asset("assets/script/app/services/ArestasService.js"); ?>"></script>
<script src="<?php echo asset("assets/script/app/services/ComandosService.js"); ?>"></script>
<script src="<?php echo asset("assets/script/app/services/LeituraService.js"); ?>"></script>
<script src="<?php echo asset("assets/script/app/services/InterfaceService.js"); ?>"></script>


<!-- Controler -->
<script src="<?php echo asset("assets/script/app/controller/HomeController.js"); ?>"></script>

</html>